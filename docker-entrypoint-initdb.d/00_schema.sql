CREATE TABLE account_roles
(
    role VARCHAR PRIMARY KEY
);

CREATE TABLE accounts
(
    login    VARCHAR PRIMARY KEY ,
    password VARCHAR NOT NULL,
    role     VARCHAR references account_roles (role)
);


CREATE TABLE tickets_status
(
    status VARCHAR PRIMARY KEY
);

CREATE TABLE tickets
(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    content TEXT NOT NULL,
    author VARCHAR NOT NULL references accounts(login),
    created timestamptz NOT NULL DEFAULT now(),
    status VARCHAR references tickets_status(status) DEFAULT 'OPEN'
);

CREATE TABLE comments
(
    id SERIAL PRIMARY KEY,
    content TEXT NOT NULL,
    ticket_id BIGINT NOT NULL references tickets(id),
    author VARCHAR NOT NULL references accounts(login),
    created timestamptz NOT NULL DEFAULT now()
);


