package org.example.authentication;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.example.entity.Role;

@Data
@RequiredArgsConstructor
public class Authentication {
  public static final Authentication ANONYMOUS = new Authentication("ANONYMOUS", null);

  private final String login;
  private final Role role;
}

