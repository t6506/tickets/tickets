package org.example.configuration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.context.ContextBeanNames;
import org.example.controller.CommentController;
import org.example.controller.TicketController;
import org.example.handler.Handler;
import org.jdbi.v3.core.Jdbi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration(proxyBeanMethods = false)
public class ApplicationConfiguration {
  @Bean
  public Jdbi jdbi() throws NamingException {
    final JndiTemplate template = new JndiTemplate();
    final DataSource dataSource = template.lookup("java:/comp/env/jdbc/db", DataSource.class);
    return Jdbi.create(dataSource);
  }

  @Bean
  public ObjectMapper objectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    objectMapper.registerModule(new JavaTimeModule());
    return objectMapper;
  }

  @Bean
  public Argon2PasswordEncoder encoder() {
    return new Argon2PasswordEncoder();
  }

  @Bean
  public PathMatcher pathMatcher() {
    return new AntPathMatcher();
  }

  @Bean(ContextBeanNames.ROUTES)
  public Map<String, Map<String, Handler>> routes(
      final TicketController ticketController,
      final CommentController commentController
      ) {
    final Map<String, Handler> get = new HashMap<>();
    get.put("/tickets", ticketController::getAll);
    get.put("/tickets/{ticketId:[0-9]+}", ticketController::getById);

    final Map<String, Handler> post = new HashMap<>();
    post.put("/tickets/create", ticketController::create);
    post.put("/tickets/{ticketId:[0-9]+}/comments/create", commentController::create);

    final Map<String, Handler> put = new HashMap<>();
    put.put("/tickets/{ticketId:[0-9]+}/close", ticketController::close);

    final Map<String, Map<String, Handler>> routes = new HashMap<>();
    routes.put("GET", get);
    routes.put("POST", post);
    routes.put("PUT", put);
    return routes;
  }
}




