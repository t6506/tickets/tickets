package org.example.context;

public class ContextAttributes {
  public static final String SPRING_CONTEXT = "org.example.context.SpringContext";
  private ContextAttributes() {
  }
}
