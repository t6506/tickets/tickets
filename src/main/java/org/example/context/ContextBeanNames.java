package org.example.context;

public class ContextBeanNames {
  public static final String ROUTES = "routes";
  public static final String PATH_MATCHER = "pathMatcher";

  private ContextBeanNames() {}
}
