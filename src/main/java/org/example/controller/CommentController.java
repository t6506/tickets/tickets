package org.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.authentication.Authentication;
import org.example.dto.CommentCreateRQ;
import org.example.dto.CommentCreateRS;
import org.example.service.CommentService;
import org.example.utils.RequestAttribute;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class CommentController {
  private final CommentService commentService;
  private final ObjectMapper objectMapper;
  private final RequestAttribute attribute;

  public void create(final HttpServletRequest rq, final HttpServletResponse rs) throws IOException {
    final Authentication authentication = attribute.getAuthentication(rq);
    long ticketId = Long.parseLong(attribute.getRequestParam(rq, "ticketId"));
    final CommentCreateRQ commentCreateRQ = objectMapper.readValue(rq.getReader(), CommentCreateRQ.class);
    final CommentCreateRS commentCreateRS = commentService.create(authentication, ticketId, commentCreateRQ);
    rs.getWriter().write(objectMapper.writeValueAsString(commentCreateRS));
  }
}
