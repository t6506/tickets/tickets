package org.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.authentication.Authentication;
import org.example.dto.TicketCloseRS;
import org.example.dto.TicketCreateRQ;
import org.example.dto.TicketCreateRS;
import org.example.dto.TicketGetAllRS;
import org.example.dto.TicketGetByIdRS;
import org.example.service.TicketService;
import org.example.utils.RequestAttribute;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class TicketController {
  private final TicketService ticketService;
  private final ObjectMapper objectMapper;
  private final RequestAttribute attribute;

  public void create(final HttpServletRequest rq, final HttpServletResponse rs) throws IOException {
    final Authentication authentication = attribute.getAuthentication(rq);
    final TicketCreateRQ ticketCreateRQ = objectMapper.readValue(rq.getReader(), TicketCreateRQ.class);
    final TicketCreateRS ticketCreateRS = ticketService.create(authentication, ticketCreateRQ);
    rs.getWriter().write(objectMapper.writeValueAsString(ticketCreateRS));
  }

  public void getAll(final HttpServletRequest rq, final HttpServletResponse rs) throws IOException {
    final Authentication authentication = attribute.getAuthentication(rq);
    final List<TicketGetAllRS> ticketGetAllRS = ticketService.getAll(authentication);
    rs.getWriter().write(objectMapper.writeValueAsString(ticketGetAllRS));
  }

  public void close(final HttpServletRequest rq, final HttpServletResponse rs) throws IOException {
    final Authentication authentication = attribute.getAuthentication(rq);
    final long id = Long.parseLong(attribute.getRequestParam(rq, "ticketId"));
    final TicketCloseRS ticketCloseRS = ticketService.close(authentication, id);
    rs.getWriter().write(objectMapper.writeValueAsString(ticketCloseRS));
  }

  public void getById(final HttpServletRequest rq, final HttpServletResponse rs) throws Exception{
    final Authentication authentication = attribute.getAuthentication(rq);
    final long ticketId = Long.parseLong(attribute.getRequestParam(rq, "ticketId"));
    final TicketGetByIdRS ticketGetByIdRS = ticketService.getById(authentication, ticketId);
    rs.getWriter().write(objectMapper.writeValueAsString(ticketGetByIdRS));
  }


}
