package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class CommentCreateRS {
  private long id;
  private String content;
  private OffsetDateTime created;
}
