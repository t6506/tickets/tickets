package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class CommentTicketsByIdDTO {
  private long id;
  private String content;
  private String author;
  private OffsetDateTime created;
}
