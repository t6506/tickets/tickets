package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.entity.Status;

@Data
@AllArgsConstructor
public class TicketCloseRS {
  private long id;
  private String name;
  private Status status;
}
