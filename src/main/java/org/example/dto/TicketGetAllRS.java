package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.entity.Status;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class TicketGetAllRS {
  private long id;
  private String name;
  private String content;
  private OffsetDateTime created;
  private Status status;
}
