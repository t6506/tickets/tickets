package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.entity.Status;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class TicketGetByIdRS {
  private long id;
  private String name;
  private String content;
  private OffsetDateTime created;
  private Status status;
  private List<CommentTicketsByIdDTO> comments;
}
