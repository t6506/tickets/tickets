package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentEntity {
  private long id;
  private long ticketId;
  private String content;
  private String author;
  private OffsetDateTime created;
}
