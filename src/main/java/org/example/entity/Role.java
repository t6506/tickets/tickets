package org.example.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Role {
  USER("USER"), SUPPORT("SUPPORT");

  private final String value;

}
