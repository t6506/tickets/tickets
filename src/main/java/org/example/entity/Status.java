package org.example.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Status {
  OPEN("OPEN"), CLOSED("CLOSED");

  private final String value;

}
