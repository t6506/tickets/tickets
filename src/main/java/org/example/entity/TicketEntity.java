package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketEntity {
  private long id;
  private String name;
  private String content;
  private String author;
  private OffsetDateTime created;
  private Status status;
  }
