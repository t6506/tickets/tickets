package org.example.exception;

public class BadEncodedBasicCredentials extends ApplicationException{
  public BadEncodedBasicCredentials() {
  }

  public BadEncodedBasicCredentials(final String message) {
    super(message);
  }

  public BadEncodedBasicCredentials(final String message, final Throwable cause) {
    super(message, cause);
  }

  public BadEncodedBasicCredentials(final Throwable cause) {
    super(cause);
  }

  public BadEncodedBasicCredentials(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
