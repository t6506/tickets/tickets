package org.example.exception;

public class NotAuthenticatedException extends ApplicationException{
  public NotAuthenticatedException() {
  }

  public NotAuthenticatedException(final String message) {
    super(message);
  }

  public NotAuthenticatedException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public NotAuthenticatedException(final Throwable cause) {
    super(cause);
  }

  public NotAuthenticatedException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
