package org.example.exception;

public class NotRequiredFieldException extends ApplicationException{
  public NotRequiredFieldException() {
  }

  public NotRequiredFieldException(final String message) {
    super(message);
  }

  public NotRequiredFieldException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public NotRequiredFieldException(final Throwable cause) {
    super(cause);
  }

  public NotRequiredFieldException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
