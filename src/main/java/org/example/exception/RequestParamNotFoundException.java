package org.example.exception;

public class RequestParamNotFoundException extends ApplicationException {
  public RequestParamNotFoundException() {
  }

  public RequestParamNotFoundException(final String message) {
    super(message);
  }

  public RequestParamNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public RequestParamNotFoundException(final Throwable cause) {
    super(cause);
  }

  public RequestParamNotFoundException(final String message, final Throwable cause, final boolean enableSuppression,
                                       final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
