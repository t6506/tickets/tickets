package org.example.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.context.ContextAttributes;
import org.example.authentication.Authentication;
import org.example.exception.BadEncodedBasicCredentials;
import org.example.service.AccountService;
import org.example.utils.RequestAttribute;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class BasicAuthenticationFilter extends HttpFilter {
  public static final String AUTHORIZATION = "Authorization";
  public static final String SCHEME = "Basic";
  public static final String DELIMITER = ":";
  private AccountService accountService;

  @Override
  public void init() {
    final ApplicationContext context = (ApplicationContext) getServletContext().getAttribute(ContextAttributes.SPRING_CONTEXT);
    accountService = context.getBean(AccountService.class);
  }

  @Override
  protected void doFilter(final HttpServletRequest rq, final HttpServletResponse rs, final FilterChain chain) throws ServletException, IOException {
    final String header = rq.getHeader(AUTHORIZATION);
    if (header == null) {
      chain.doFilter(rq, rs);
      return;
    }

    final String encodedLoginAndPassword = header.substring(SCHEME.length() + 1);
    final String loginAndPassword = new String(Base64.getDecoder().decode(encodedLoginAndPassword), StandardCharsets.UTF_8);
    final String[] parts = loginAndPassword.split(DELIMITER);
    if (parts.length != 2) {
      throw new BadEncodedBasicCredentials();
    }

    final String login = parts[0];
    final String password = parts[1];

    final Authentication authenticate = accountService.authenticate(login, password);
    rq.setAttribute(RequestAttribute.AUTHENTICATION, authenticate);
    chain.doFilter(rq, rs);
  }
}
