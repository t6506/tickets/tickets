package org.example.listener;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.example.context.ContextAttributes;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.support.GenericApplicationContext;


public class ContextLoadDestroyListener implements ServletContextListener {

  private ConfigurableApplicationContext springContext;

  @Override
  public void contextInitialized(final ServletContextEvent sce) {

    springContext = new GenericApplicationContext();
    final ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner((GenericApplicationContext) springContext);
    scanner.scan("org.example");

    springContext.refresh();

    sce.getServletContext().setAttribute(
        ContextAttributes.SPRING_CONTEXT,
        springContext
    );
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    if (springContext != null) {
      springContext.close();
    }
  }
}
