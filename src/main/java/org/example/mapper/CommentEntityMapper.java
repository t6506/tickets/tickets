package org.example.mapper;

import org.example.authentication.Authentication;
import org.example.dto.CommentCreateRQ;
import org.example.dto.CommentCreateRS;
import org.example.dto.CommentTicketsByIdDTO;
import org.example.entity.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

@Mapper
@Service
public interface CommentEntityMapper {

  CommentTicketsByIdDTO toCommentTicketsByIdDTO(CommentEntity entity);

  @Mapping(source = "authentication.login", target = "author")
  CommentEntity fromCommentCreateRQ(Authentication authentication, long ticketId, CommentCreateRQ rq);

  CommentCreateRS toCommentCreateRS(CommentEntity entity);
}
