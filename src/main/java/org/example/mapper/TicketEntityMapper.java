package org.example.mapper;

import org.example.authentication.Authentication;
import org.example.dto.CommentTicketsByIdDTO;
import org.example.dto.TicketCloseRS;
import org.example.dto.TicketCreateRQ;
import org.example.dto.TicketCreateRS;
import org.example.dto.TicketGetAllRS;
import org.example.dto.TicketGetByIdRS;
import org.example.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface TicketEntityMapper {

  @Mapping(source = "authentication.login", target = "author")
  TicketEntity fromTicketCreateRQ(final Authentication authentication, final TicketCreateRQ rq);

  TicketCreateRS toTicketCreateRS(TicketEntity entity);

  TicketCloseRS toTicketCloseRS(TicketEntity entity);

  TicketGetAllRS toTicketGetAllRS(TicketEntity entity);

  @Mapping(source = "comments", target = "comments")
  TicketGetByIdRS toTicketGetByIdRS(TicketEntity saved, List<CommentTicketsByIdDTO> comments);
}
