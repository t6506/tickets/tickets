package org.example.repository;

import lombok.RequiredArgsConstructor;
import org.example.entity.AccountEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class AccountRepository {
  private final Jdbi jdbi;

  public Optional<AccountEntity> findByLogin(final String login) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "SELECT login, password, role  FROM accounts WHERE login = :login"
        )
        .bind("login", login)
        .mapToBean(AccountEntity.class)
        .findFirst());
  }
}
