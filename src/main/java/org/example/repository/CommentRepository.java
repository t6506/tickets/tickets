package org.example.repository;

import lombok.RequiredArgsConstructor;
import org.example.entity.CommentEntity;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.RowMapper;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;


@Repository
@RequiredArgsConstructor
public class CommentRepository {
  private final Jdbi jdbi;

  private final RowMapper<CommentEntity> commentMapper = (rs, ctx) ->
      new CommentEntity(
          rs.getLong("id"),
          rs.getLong("ticket_id"),
          rs.getString("content"),
          rs.getString("author"),
          rs.getObject("created", OffsetDateTime.class)
      );

  public List<CommentEntity> getByTicketId(final long ticketId) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "SELECT id, ticket_id, author, content, created\n" +
                "FROM comments\n" +
                "WHERE ticket_id = :id\n" +
                "ORDER BY created;"
        )
        .bind("id", ticketId)
        .map(commentMapper)
        .list());
  }

  public CommentEntity create(final CommentEntity comment) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "INSERT INTO comments (ticket_id, content, author)\n" +
                "VALUES (:ticket_id, :content, :author)\n" +
                "RETURNING id, ticket_id, content, author, created"
        )
        .bind("ticket_id", comment.getTicketId())
        .bind("content", comment.getContent())
        .bind("author", comment.getAuthor())
        .map(commentMapper)
        .one());
  }
}
