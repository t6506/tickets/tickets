package org.example.repository;

import lombok.RequiredArgsConstructor;
import org.example.entity.CommentEntity;
import org.example.entity.Status;
import org.example.entity.TicketEntity;
import org.example.exception.ItemNotFoundException;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.RowMapper;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;


@Repository
@RequiredArgsConstructor
public class TicketRepository {
  private final Jdbi jdbi;

  private final RowMapper<TicketEntity> ticketMapper = (rs, ctx) ->
      new TicketEntity(
          rs.getLong("id"),
          rs.getString("name"),
          rs.getString("content"),
          rs.getString("author"),
          rs.getObject("created", OffsetDateTime.class),
          Status.valueOf(rs.getString("status"))
      );

  private final RowMapper<CommentEntity> commentMapper = (rs, ctx) ->
      new CommentEntity(
          rs.getLong("id"),
          rs.getLong("ticket_id"),
          rs.getString("content"),
          rs.getString("author"),
          rs.getObject("created", OffsetDateTime.class)
      );

  public TicketEntity create(final TicketEntity ticket) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "INSERT INTO tickets (name, content, author)\n" +
                "VALUES (:name, :content, :author)\n" +
                "RETURNING id, name, content, author, created, status;"
        )
        .bind("name", ticket.getName())
        .bind("content", ticket.getContent())
        .bind("author", ticket.getAuthor())
        .map(ticketMapper)
        .one());
  }

  public TicketEntity close(final long ticketId) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "UPDATE tickets SET status = 'CLOSED' " +
                "WHERE id = :id\n" +
                "RETURNING id, name, content, author, created, status;"
        )
        .bind("id", ticketId)
        .map(ticketMapper)
        .one());
  }

  public List<TicketEntity> getAll() {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "SELECT id, name, content, author, created, status\n" +
                "FROM tickets;"
        )
        .map(ticketMapper)
        .list());
  }

  public List<TicketEntity> getByAuthor(final String author) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "SELECT id, name, content, author, created, status\n" +
                "FROM tickets\n" +
                "WHERE author = :author;"
        )
        .bind("author", author)
        .map(ticketMapper)
        .list());
  }

  public TicketEntity getById(final long ticketId) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "SELECT id, name, content, author, created, status\n" +
                "FROM tickets\n" +
                "WHERE id = :id;"
        )
        .bind("id", ticketId)
        .map(ticketMapper)
        .findFirst()
        .orElseThrow(() -> new ItemNotFoundException("ticketId: " + ticketId)));
  }
}
