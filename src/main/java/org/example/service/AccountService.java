package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.authentication.Authentication;
import org.example.entity.AccountEntity;
import org.example.repository.AccountRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
public class AccountService {
  private final PasswordEncoder encoder;
  private final AccountRepository repository;

  public Authentication authenticate(final String login, final String password) {
    final Optional<AccountEntity> account = repository.findByLogin(login);

    if (!account.isPresent()) {
      return Authentication.ANONYMOUS;
    }

    if (!encoder.matches(password, account.get().getPassword())) {
      return Authentication.ANONYMOUS;
    }

    return new Authentication(
        account.get().getLogin(),
        account.get().getRole()
    );
  }
}
