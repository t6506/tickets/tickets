package org.example.service;


import lombok.RequiredArgsConstructor;
import org.example.authentication.Authentication;
import org.example.dto.CommentCreateRQ;
import org.example.dto.CommentCreateRS;
import org.example.entity.CommentEntity;
import org.example.entity.Role;
import org.example.entity.TicketEntity;
import org.example.exception.NotAuthorizedException;
import org.example.mapper.CommentEntityMapper;
import org.example.repository.CommentRepository;
import org.example.repository.TicketRepository;
import org.example.utils.RequiredFields;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommentService {
  private final CommentRepository commentRepository;
  private final CommentEntityMapper commentMapper;
  private final TicketRepository ticketRepository;
  private final RequiredFields requiredFields;

  public CommentCreateRS create(final Authentication authentication, final long ticketId, final CommentCreateRQ rq) {
    requiredFields.check(rq);
    final String login = authentication.getLogin();
    final Role role = authentication.getRole();
    final TicketEntity ticket = ticketRepository.getById(ticketId);
    final String author = ticket.getAuthor();
    if (role.equals(Role.USER) && !author.equals(login)) {
      throw new NotAuthorizedException("login: " + login);
    }
    final CommentEntity comment = commentMapper.fromCommentCreateRQ(authentication, ticketId, rq);
    final CommentEntity saved = commentRepository.create(comment);
    return commentMapper.toCommentCreateRS(saved);
  }
}
