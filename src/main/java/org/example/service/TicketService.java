package org.example.service;


import lombok.RequiredArgsConstructor;
import org.example.authentication.Authentication;
import org.example.dto.CommentTicketsByIdDTO;
import org.example.dto.TicketCloseRS;
import org.example.dto.TicketCreateRQ;
import org.example.dto.TicketCreateRS;
import org.example.dto.TicketGetAllRS;
import org.example.dto.TicketGetByIdRS;
import org.example.entity.Role;
import org.example.entity.TicketEntity;
import org.example.exception.NotAuthorizedException;
import org.example.mapper.CommentEntityMapper;
import org.example.mapper.TicketEntityMapper;
import org.example.repository.CommentRepository;
import org.example.repository.TicketRepository;
import org.example.utils.RequiredFields;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TicketService {
  private final TicketRepository ticketRepository;
  private final CommentRepository commentRepository;
  private final TicketEntityMapper ticketMapper;
  private final CommentEntityMapper commentMapper;
  private final RequiredFields requiredFields;

  public TicketCreateRS create(final Authentication authentication, final TicketCreateRQ rq) {
    if (authentication.getRole().equals(Role.SUPPORT)) {
      throw new NotAuthorizedException(authentication.getRole().toString());
    }
    requiredFields.check(rq);
    final TicketEntity ticket = ticketMapper.fromTicketCreateRQ(authentication, rq);
    final TicketEntity saved = ticketRepository.create(ticket);
    return ticketMapper.toTicketCreateRS(saved);
  }

  public List<TicketGetAllRS> getAll(final Authentication authentication) {
    List<TicketEntity> tickets;
    if (authentication.getRole().equals(Role.SUPPORT)) {
      tickets = ticketRepository.getAll();
    }
    else {
      tickets = ticketRepository.getByAuthor(authentication.getLogin());
    }
    return tickets.stream().map(ticketMapper::toTicketGetAllRS).collect(Collectors.toList());
  }

  public TicketCloseRS close(final Authentication authentication, final long ticketId) {
    final TicketEntity saved = ticketRepository.getById(ticketId);
    if (!saved.getAuthor().equals(authentication.getLogin())) {
      throw new NotAuthorizedException("login: " + authentication.getLogin());
    }

    TicketEntity closed = ticketRepository.close(ticketId);
    return ticketMapper.toTicketCloseRS(closed);
  }

  public TicketGetByIdRS getById(final Authentication authentication, final long ticketId) {
    final String login = authentication.getLogin();
    final Role role = authentication.getRole();
    final TicketEntity saved = ticketRepository.getById(ticketId);
    final String author = saved.getAuthor();
    if (role.equals(Role.USER) && !author.equals(login)) {
      throw new NotAuthorizedException("login: " + login);
    }
    List<CommentTicketsByIdDTO> comments = commentRepository.getByTicketId(ticketId).stream()
        .map(commentMapper::toCommentTicketsByIdDTO)
        .collect(Collectors.toList());
    return ticketMapper.toTicketGetByIdRS(saved, comments);
  }
}
