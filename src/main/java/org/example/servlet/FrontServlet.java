package org.example.servlet;

import com.fasterxml.jackson.databind.JsonMappingException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.example.context.ContextAttributes;
import org.example.authentication.Authentication;
import org.example.context.ContextBeanNames;
import org.example.exception.ItemNotFoundException;
import org.example.exception.NotAuthenticatedException;
import org.example.exception.NotAuthorizedException;
import org.example.exception.NotRequiredFieldException;
import org.example.exception.RequestParamNotFoundException;
import org.example.handler.Handler;
import org.example.utils.RequestAttribute;
import org.springframework.context.ApplicationContext;
import org.springframework.util.PathMatcher;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Slf4j
public class FrontServlet extends HttpServlet {
  private Map<String, Map<String, Handler>> httpRoutes;
  private PathMatcher pathMatcher;
  private final Handler badRequestHandler = (rq, rs) -> rs.setStatus(400);
  private final Handler notFoundHandler = (rq, rs) -> rs.setStatus(404);
  private final Handler methodNotAllowedHandler = (rq, rs) -> rs.setStatus(405);
  private final Handler internalServerErrorHandler = (rq, rs) -> rs.setStatus(500);

  @Override
  public void init() throws ServletException {
    final ApplicationContext context = (ApplicationContext) getServletContext().getAttribute(ContextAttributes.SPRING_CONTEXT);
    httpRoutes = (Map<String, Map<String, Handler>>) context.getBean(ContextBeanNames.ROUTES);
    pathMatcher = (PathMatcher) context.getBean(ContextBeanNames.PATH_MATCHER);
  }

  @Override
  protected void service(final HttpServletRequest rq, final HttpServletResponse rs) throws ServletException, IOException {

    try {
      handle(rq, rs);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void handle(final HttpServletRequest rq, final HttpServletResponse rs) throws Exception {
    final String path = rq.getRequestURI().substring(rq.getContextPath().length());
    final String method = rq.getMethod();
    final Handler handler = getHandler(path, method);
    final Map<String, String> requestParams = getRequestParams(path, method);
    rq.setAttribute(RequestAttribute.REQUEST_PARAMS, requestParams);
    final Authentication authentication = (Authentication) rq.getAttribute(RequestAttribute.AUTHENTICATION);
    try {
      if (authentication.equals(Authentication.ANONYMOUS)) {
        throw new NotAuthenticatedException(authentication.getLogin());
      }
      handler.handle(rq, rs);
    }
    catch (NotAuthorizedException | NotAuthenticatedException e) {
        log.info(e.getMessage(), e);
        notFoundHandler.handle(rq, rs);
    }
    catch (RequestParamNotFoundException | ItemNotFoundException | JsonMappingException | NotRequiredFieldException e) {
      log.info(e.getMessage(), e);
      badRequestHandler.handle(rq, rs);
    }
    catch (Exception e) {
        log.info(e.toString());
        internalServerErrorHandler.handle(rq, rs);
    }
  }

  private Map<String, String> getRequestParams(final String path, final String method) {
    if (!httpRoutes.containsKey(method)) {
      return Collections.emptyMap();
    }
    return httpRoutes.get(method).keySet()
        .stream()
        .filter(pattern -> pathMatcher.match(pattern, path))
        .findFirst()
        .map(pattern -> pathMatcher.extractUriTemplateVariables(pattern, path))
        .orElse(Collections.emptyMap());
  }

  private Handler getHandler(final String path, final String method) {
    if (!httpRoutes.containsKey(method)) {
      return methodNotAllowedHandler;
    }
    return httpRoutes.get(method).entrySet()
        .stream()
        .filter(o -> pathMatcher.match(o.getKey(), path))
        .map(Map.Entry::getValue)
        .findFirst()
        .orElse(notFoundHandler);
  }
}
