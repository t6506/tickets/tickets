package org.example.utils;

import jakarta.servlet.http.HttpServletRequest;
import org.example.authentication.Authentication;
import org.example.exception.RequestParamNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class RequestAttribute {
  public static final String REQUEST_PARAMS = "org.example.utils.RequestAttribute.requestParams";
  public static final String AUTHENTICATION = "org.example.entity.authenticate.Authentication";

  public Authentication getAuthentication(final HttpServletRequest rq) {
    return (Authentication) rq.getAttribute(AUTHENTICATION);
  }

  public String getRequestParam(final HttpServletRequest rq, final String param) {
    Map<String, String> requestParam = (Map<String, String>) rq.getAttribute(REQUEST_PARAMS);
    return Optional.ofNullable(requestParam.get(param)).orElseThrow(() -> new RequestParamNotFoundException(param));
  }
}
