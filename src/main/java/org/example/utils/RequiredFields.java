package org.example.utils;

import org.example.exception.ApplicationException;
import org.example.exception.NotRequiredFieldException;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;

@Service
public class RequiredFields {
  public void check(final Object object) {
    final ArrayList<String> nullFields = new ArrayList<>();
    final Field[] fields = object.getClass().getDeclaredFields();
    try {
      for (Field field : fields) {
        if (!field.isAccessible()) {
          field.setAccessible(true);
        }
        if (field.get(object) == null) {
          nullFields.add(field.getName());
        }
      }
    } catch (IllegalAccessException e) {
      throw new ApplicationException(e);
    }
    if (!nullFields.isEmpty()) {
      throw new NotRequiredFieldException(nullFields.toString());
    }
  }
}
